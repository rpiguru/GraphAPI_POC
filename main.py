import urllib2
import token_reader
import yaml

token_list = token_reader.get_token()
token = token_list['accessToken']

req = urllib2.Request('https://graph.windows.net/getcloudsafe.com/users?api-version=1.6')
req.add_header('Authorization', token)
resp = urllib2.urlopen(req)

content = resp.read()
result = yaml.load(content)

print 'Metadata: ', result['odata.metadata']
print " --- Users ---"
for user in result['value']:
    # for item in user.items():
    #     print item
    print 'User name: ', user['displayName']
    print 'Mail: ', user['mail']
    print 'Last Login Time: ', user['refreshTokensValidFromDateTime']
    if len(user['assignedLicenses']) > 0:
        print 'Found license'
        for lic in user['assignedLicenses']:
            sku_id = lic['skuId']

    print ""


